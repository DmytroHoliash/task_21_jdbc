package com.epam.trainings.error;

public class NoSuchTableException extends RuntimeException {

  public NoSuchTableException() {
  }

  public NoSuchTableException(String message) {
    super(message);
  }

  public NoSuchTableException(String message, Throwable cause) {
    super(message, cause);
  }

  public NoSuchTableException(Throwable cause) {
    super(cause);
  }

}
