package com.epam.trainings;

import com.epam.trainings.view.MyView;

public class App {

  public static void main(String[] args) {
    new MyView().show();
  }
}
