package com.epam.trainings.model.person;

import com.epam.trainings.annotations.Column;
import com.epam.trainings.annotations.Table;

@Table(name = "employee")
public class Employee extends Person {

  @Column(name = "attraction_id")
  private int attractionId;

  public int getAttractionId() {
    return attractionId;
  }

  public void setAttractionId(int attractionId) {
    this.attractionId = attractionId;
  }

  @Override
  public String toString() {
    return "Employee{" + super.toString() +
        "attractionId=" + attractionId +
        '}';
  }
}
