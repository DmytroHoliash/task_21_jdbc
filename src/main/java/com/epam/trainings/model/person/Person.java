package com.epam.trainings.model.person;

import com.epam.trainings.annotations.Column;
import com.epam.trainings.annotations.Id;

public abstract class Person {

  @Id
  @Column(name = "id")
  private int id;

  @Column(name = "name")
  private String name;

  @Column(name = "surname")
  private String surname;

  @Column(name = "park_id")
  private int parkId;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public int getParkId() {
    return parkId;
  }

  public void setParkId(int parkId) {
    this.parkId = parkId;
  }

  @Override
  public String toString() {
    return "id=" + id +
        ", name='" + name + '\'' +
        ", surname='" + surname + '\'' +
        '}';
  }
}
