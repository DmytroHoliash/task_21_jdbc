package com.epam.trainings.model.person;

import com.epam.trainings.annotations.Column;
import com.epam.trainings.annotations.Table;

@Table(name = "actor")
public class Actor extends Person {

  @Column(name = "show_id")
  private int showId;

  public int getShowId() {
    return showId;
  }

  public void setShowId(int showId) {
    this.showId = showId;
  }

  @Override
  public String toString() {
    return "Actor{" + super.toString() +
        "showId=" + showId +
        '}';
  }
}
