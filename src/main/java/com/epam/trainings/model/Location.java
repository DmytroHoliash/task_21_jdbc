package com.epam.trainings.model;

import com.epam.trainings.annotations.Column;
import com.epam.trainings.annotations.Id;
import com.epam.trainings.annotations.Table;

@Table(name = "location")
public class Location {

  @Id
  @Column(name = "id")
  private int id;

  @Column(name = "country")
  private String country;

  @Column(name = "city")
  private String city;

  @Column(name = "street")
  private String street;

  public Location() {
  }

  public Location(int id, String country, String city, String street) {
    this.id = id;
    this.country = country;
    this.city = city;
    this.street = street;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  @Override
  public String toString() {
    return String
        .format("|%-5d|%-15s|%-15s|%-15s|\n+-----+---------------+---------------+---------------+",
            id, country, city, street);
  }
}
