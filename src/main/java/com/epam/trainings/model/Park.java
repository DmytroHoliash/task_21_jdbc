package com.epam.trainings.model;

import com.epam.trainings.annotations.Column;
import com.epam.trainings.annotations.Id;
import com.epam.trainings.annotations.Table;

@Table(name = "park")
public class Park {

  @Id
  @Column(name = "id")
  private int id;

  @Column(name = "max_clients")
  private int maxClients;

  @Column(name = "location_id")
  private int locationId;

  @Column(name = "queue_option")
  private boolean queueOption;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getMaxClients() {
    return maxClients;
  }

  public void setMaxClients(int maxClients) {
    this.maxClients = maxClients;
  }

  public int getLocationId() {
    return locationId;
  }

  public void setLocationId(int locationId) {
    this.locationId = locationId;
  }

  public boolean isQueueOption() {
    return queueOption;
  }

  public Park() {
  }

  public Park(int id, int maxClients, int locationId, boolean queueOption) {
    this.id = id;
    this.maxClients = maxClients;
    this.locationId = locationId;
    this.queueOption = queueOption;
  }

  public void setQueueOption(boolean queueOption) {
    this.queueOption = queueOption;
  }

  @Override
  public String toString() {
    return String
        .format("|%-5d|%-11d|%-11d|%-12b|\n+-----+-----------+-----------+------------+", id,
            maxClients, locationId, queueOption);
  }
}
