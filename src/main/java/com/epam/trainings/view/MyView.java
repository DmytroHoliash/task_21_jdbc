package com.epam.trainings.view;

import com.epam.trainings.dao.impl.LocationDaoImpl;
import com.epam.trainings.dao.impl.ParkDaoImpl;
import com.epam.trainings.model.Location;
import com.epam.trainings.model.Park;
import com.epam.trainings.persistant.ConnectionManager;
import com.epam.trainings.service.LocationService;
import com.epam.trainings.service.ParkService;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.TreeMap;

public class MyView {

  private final Map<String, String> menu;
  private final Map<String, Printable> menuMethods;
  private final ResourceBundle bundle = ResourceBundle.getBundle("menu");
  private final Scanner scanner = new Scanner(System.in);

  public MyView() {
    menu = new TreeMap<>();
    menuMethods = new HashMap<>();
    Enumeration<String> keys = bundle.getKeys();
    while (keys.hasMoreElements()) {
      String key = keys.nextElement();
      menu.put(key, bundle.getString(key));
    }
    menuMethods.put("A", this::showDBStructure);
    menuMethods.put("11", this::selectPark);
    menuMethods.put("12", this::createPark);
    menuMethods.put("13", this::updatePark);
    menuMethods.put("14", this::deletePark);
    menuMethods.put("15", this::setMaxClients);
    menuMethods.put("21", this::selectLocation);
    menuMethods.put("22", this::createLocation);
    menuMethods.put("23", this::updateLocation);
    menuMethods.put("24", this::deleteLocation);
    menuMethods.put("25", this::updateStreet);
  }


  private void showDBStructure() {
    String database = "disneydb";
    String[] types = {"TABLE"};
    try {
      DatabaseMetaData metaData = ConnectionManager.getConnection().getMetaData();
      try (ResultSet tables = metaData.getTables(database, null, "%", types)) {
        while (tables.next()) {
          String table = tables.getString(3);
          System.out.println("Table: " + table);
          try (ResultSet cols = metaData.getColumns(database, null, table, "%")) {
            System.out.println("Columns: ");
            System.out.print("| ");
            while (cols.next()) {
              System.out.print(cols.getString(4) + " : " + cols.getString(6) + " | ");
            }
          }
          System.out.println();
          System.out.println();
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  private void selectPark() {
    ParkService parkService = new ParkService(new ParkDaoImpl());
    try {
      System.out.println("+-----+-----------+-----------+------------+");
      System.out.println(String
          .format("|%-5s|%-11s|%-11s|%-12s|", "id", "max_clients", "location_id", "queue_option"));
      System.out.println("+-----+-----------+-----------+------------+");
      parkService.findAll().forEach(System.out::println);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  private void createPark() {
    System.out.println("Input id, max_clients, location_id, queue_option");
    int id = scanner.nextInt();
    int max = scanner.nextInt();
    int location = scanner.nextInt();
    boolean queue = scanner.nextBoolean();
    Park park = new Park(id, max, location, queue);
    try {
      int count = new ParkService(new ParkDaoImpl()).create(park);
      System.out.printf("%1d row was created\n", count);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  private void deletePark() {
    System.out.println("Input park id: ");
    int id = scanner.nextInt();
    try {
      int count = new ParkService(new ParkDaoImpl()).delete(id);
      System.out.printf("%1d row was deleted\n", count);
    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }
  }

  private void updatePark() {
    System.out.println("Input id, new max_clients, new location_id, new queue_option");
    int id = scanner.nextInt();
    int max = scanner.nextInt();
    int location = scanner.nextInt();
    boolean queue = scanner.nextBoolean();
    Park park = new Park(id, max, location, queue);
    try {
      int count = new ParkService(new ParkDaoImpl()).create(park);
      System.out.printf("%1d row was updated\n", count);
    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }
  }

  private void setMaxClients() {
    System.out.println("Input id and new max_clients");
    int id = scanner.nextInt();
    int max = scanner.nextInt();
    int count = 0;
    try {
      count = new ParkService(new ParkDaoImpl()).setMaxClients(id, max);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    System.out.printf("%1d row was updated\n", count);
  }

  private void selectLocation() {
    LocationService locationService = new LocationService(new LocationDaoImpl());
    try {
      System.out.println("+-----+---------------+---------------+---------------+");
      System.out.println(String
          .format("|%-5s|%-15s|%-15s|%-15s|", "id", "country", "city", "street"));
      System.out.println("+-----+---------------+---------------+---------------+");
      locationService.findAll().forEach(System.out::println);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  private void createLocation() {
    System.out.println("Input id, country, city, street:");
    int id = scanner.nextInt();
    String country = scanner.nextLine();
    String city = scanner.nextLine();
    String street = scanner.nextLine();
    Location location = new Location(id, country, city, street);
    try {
      int count = new LocationService(new LocationDaoImpl()).create(location);
      System.out.printf("%1d row was created\n", count);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  private void deleteLocation() {
    System.out.println("Input location id: ");
    int id = scanner.nextInt();
    try {
      int count = new LocationService(new LocationDaoImpl()).delete(id);
      System.out.printf("%1d row was deleted\n", count);
    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }
  }

  private void updateLocation() {
    System.out.println("Input id, new country, new city, new street: ");
    int id = scanner.nextInt();
    String country = scanner.nextLine();
    String city = scanner.nextLine();
    String street = scanner.nextLine();
    Location location = new Location(id, country, city, street);
    try {
      int count = new LocationService(new LocationDaoImpl()).create(location);
      System.out.printf("%1d row was updated\n", count);
    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }
  }

  private void updateStreet() {
    System.out.println("Input id and new street: ");
    int id = scanner.nextInt();
    scanner.nextLine();
    String street = scanner.nextLine();
    try {
      int count = new LocationService(new LocationDaoImpl()).updateStreet(id, street);
      System.out.printf("%1d row was updated\n", count);
    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }
  }

  private void outputMenu() {
    System.out.println("\nMENU:");
    for (String key : menu.keySet()) {
      if (key.length() == 1) {
        System.out.printf("%s - %s\n", key, menu.get(key));
      }
    }
  }

  private void outputSubMenu(String fig) {
    System.out.println("\nSubMENU:");
    for (String key : menu.keySet()) {
      if (key.length() != 1 && key.substring(0, 1).equals(fig)) {
        System.out.printf("%s - %s\n", key, menu.get(key));
      }
    }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please, select menu point.");
      keyMenu = scanner.nextLine().toUpperCase();
      if (keyMenu.matches("^\\d")) {
        outputSubMenu(keyMenu);
        System.out.println("Please, select menu point.");
        keyMenu = scanner.nextLine().toUpperCase();
      }
      try {
        menuMethods.get(keyMenu).print();
      } catch (Exception e) {
        System.out.println(e.getMessage());
      }
    } while (!keyMenu.equals("Q"));
  }
}
