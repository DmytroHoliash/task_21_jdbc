package com.epam.trainings.view;

@FunctionalInterface
public interface Printable {
  void print();
}
