package com.epam.trainings.service;

import com.epam.trainings.dao.LocationDao;
import com.epam.trainings.model.Location;
import java.sql.SQLException;
import java.util.List;

public class LocationService {

  private LocationDao locationDao;

  public LocationService(LocationDao locationDao) {
    this.locationDao = locationDao;
  }

  public List<Location> findAll() throws SQLException {
    return this.locationDao.getAll();
  }

  public Location findById(int id) throws SQLException {
    return this.locationDao.findByID(id);
  }

  public int create(Location entity) throws SQLException {
    return this.locationDao.create(entity);
  }

  public int delete(int id) throws SQLException {
    return this.locationDao.delete(id);
  }

  public int update(Location entity) throws SQLException {
    return this.locationDao.update(entity);
  }

  public int updateStreet(int id, String newStreet) throws SQLException {
    return this.locationDao.updateStreet(id, newStreet);
  }
}
