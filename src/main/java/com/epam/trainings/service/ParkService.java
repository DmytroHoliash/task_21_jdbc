package com.epam.trainings.service;

import com.epam.trainings.dao.ParkDao;
import com.epam.trainings.model.Park;
import java.sql.SQLException;
import java.util.List;

public class ParkService {

  private ParkDao parkDao;

  public ParkService(ParkDao parkDao) {
    this.parkDao = parkDao;
  }

  public List<Park> findAll() throws SQLException {
    return this.parkDao.getAll();
  }

  public Park findById(int id) throws SQLException {
    return this.parkDao.findByID(id);
  }

  public int update(Park entity) throws SQLException {
    return this.parkDao.update(entity);
  }

  public int delete(int id) throws SQLException {
    return this.parkDao.delete(id);
  }

  public int create(Park entity) throws SQLException {
    return this.parkDao.create(entity);
  }

  public int setMaxClients(int id, int newMax) throws SQLException {
    return this.parkDao.setMaxClients(id, newMax);
  }
}
