package com.epam.trainings.dao;

import com.epam.trainings.model.Park;
import java.sql.SQLException;

public interface ParkDao extends Dao<Park> {

  int setMaxClients(int id, int newMaxClients) throws SQLException;
}
