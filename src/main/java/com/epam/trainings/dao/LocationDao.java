package com.epam.trainings.dao;

import com.epam.trainings.model.Location;
import java.sql.SQLException;

public interface LocationDao extends Dao<Location> {
  int updateStreet(int id, String newStreet) throws SQLException;
}
