package com.epam.trainings.dao;

import java.sql.SQLException;
import java.util.List;

public interface Dao<T> {

  List<T> getAll() throws SQLException;

  int update(T entity) throws SQLException;

  int delete(int id) throws SQLException;

  int create(T entity) throws SQLException;

  T findByID(int id) throws SQLException;
}
