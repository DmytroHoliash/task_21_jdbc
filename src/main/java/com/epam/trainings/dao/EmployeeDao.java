package com.epam.trainings.dao;

import com.epam.trainings.model.person.Employee;

public interface EmployeeDao extends Dao<Employee> {

}
