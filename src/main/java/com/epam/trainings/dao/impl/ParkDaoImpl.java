package com.epam.trainings.dao.impl;

import com.epam.trainings.dao.ParkDao;
import com.epam.trainings.model.Park;
import com.epam.trainings.persistant.ConnectionManager;
import com.epam.trainings.transformer.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ParkDaoImpl implements ParkDao {

  private final ResourceBundle bundle = ResourceBundle.getBundle("query/park");

  @Override
  public int setMaxClients(int id, int newMaxClients) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(bundle.getString("UPDATE_CLIENTS"))) {
      ps.setInt(1, newMaxClients);
      ps.setInt(2, id);
      return ps.executeUpdate();
    }
  }

  @Override
  public List<Park> getAll() throws SQLException {
    List<Park> parks = new ArrayList<>();
    Connection connection = ConnectionManager.getConnection();
    try (Statement statement = connection.createStatement()) {
      try (ResultSet resultSet = statement.executeQuery(bundle.getString("GET_ALL"))) {
        while (resultSet.next()) {
          parks.add((Park) new Transformer<>(Park.class).transformToEntity(resultSet));
        }
      }
    }
    return parks;
  }

  @Override
  public int update(Park entity) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(bundle.getString("UPDATE"))) {
      ps.setInt(1, entity.getMaxClients());
      ps.setInt(2, entity.getLocationId());
      ps.setBoolean(3, entity.isQueueOption());
      ps.setInt(4, entity.getId());
      return ps.executeUpdate();
    }
  }

  @Override
  public int delete(int id) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(bundle.getString("DELETE"))) {
      ps.setInt(1, id);
      return ps.executeUpdate();
    }
  }

  @Override
  public int create(Park entity) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(bundle.getString("CREATE"))) {
      ps.setInt(1, entity.getId());
      ps.setInt(2, entity.getMaxClients());
      ps.setInt(3, entity.getLocationId());
      ps.setBoolean(4, entity.isQueueOption());
      return ps.executeUpdate();
    }
  }

  @Override
  public Park findByID(int id) throws SQLException {
    Park park = null;
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(bundle.getString("FIND_BY_ID"))) {
      ps.setInt(1, id);
      try (ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          park = (Park) new Transformer<>(Park.class).transformToEntity(rs);
        }
      }
    }
    return park;
  }
}
