package com.epam.trainings.dao.impl;

import com.epam.trainings.dao.LocationDao;
import com.epam.trainings.model.Location;
import com.epam.trainings.persistant.ConnectionManager;
import com.epam.trainings.transformer.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class LocationDaoImpl implements LocationDao {

  private final ResourceBundle bundle = ResourceBundle.getBundle("query/location");

  @Override
  public List<Location> getAll() throws SQLException {
    List<Location> locations = new ArrayList<>();
    Connection connection = ConnectionManager.getConnection();
    try (Statement statement = connection.createStatement()) {
      try (ResultSet resultSet = statement.executeQuery(bundle.getString("GET_ALL"))) {
        while (resultSet.next()) {
          locations.add((Location) new Transformer<>(Location.class).transformToEntity(resultSet));
        }
      }
    }
    return locations;
  }

  @Override
  public int update(Location entity) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(bundle.getString("UPDATE"))) {
      ps.setString(1, entity.getCountry());
      ps.setString(2, entity.getCity());
      ps.setString(3, entity.getStreet());
      ps.setInt(4, entity.getId());
      return ps.executeUpdate();
    }
  }

  @Override
  public int delete(int id) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(bundle.getString("DELETE"))) {
      ps.setInt(1, id);
      return ps.executeUpdate();
    }
  }

  @Override
  public int create(Location entity) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(bundle.getString("CREATE"))) {
      ps.setInt(1, entity.getId());
      ps.setString(2, entity.getCountry());
      ps.setString(3, entity.getCity());
      ps.setString(4, entity.getStreet());
      return ps.executeUpdate();
    }
  }

  @Override
  public Location findByID(int id) throws SQLException {
    Location location = null;
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(bundle.getString("FIND_BY_ID"))) {
      ps.setInt(1, id);
      try (ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          location = (Location) new Transformer<>(Location.class).transformToEntity(rs);
        }
      }
    }
    return location;
  }

  @Override
  public int updateStreet(int id, String newStreet) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(bundle.getString("UPDATE_STREET"))) {
      ps.setString(1, newStreet);
      ps.setInt(2, id);
      return ps.executeUpdate();
    }
  }
}
