package com.epam.trainings.transformer;

import com.epam.trainings.annotations.Column;
import com.epam.trainings.annotations.Table;
import com.epam.trainings.error.NoSuchTableException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Transformer<T> {

  private final Class<T> clazz;

  public Transformer(Class<T> clazz) {
    this.clazz = clazz;
  }

  public Object transformToEntity(ResultSet rs) {
    if (clazz.isAnnotationPresent(Table.class)) {
      try {
        Object entity = clazz.getConstructor().newInstance();
        for (Field f : clazz.getDeclaredFields()) {
          if (f.isAnnotationPresent(Column.class)) {
            Column column = f.getAnnotation(Column.class);
            String name = column.name();
            f.setAccessible(true);
            Type t = f.getType();
            if (t == int.class) {
              f.set(entity, rs.getInt(name));
            } else if (t == String.class) {
              f.set(entity, rs.getString(name));
            } else if (t == boolean.class) {
              f.set(entity, rs.getBoolean(name));
            }
          }
        }
        return entity;
      } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | SQLException e) {
        e.printStackTrace();
      }
    }
    throw new NoSuchTableException("Object is not entity");
  }
}
